package org.expropiados.cloudservices.security.modules.login.application;

import org.expropiados.cloudservices.security.modules.login.application.port.out.GetUserByEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.SendResetPasswordEmailPort;
import org.expropiados.cloudservices.security.modules.login.domain.roles.User;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateChangePasswordTokenUseCase;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Tag("UnitTest")
class ForgotPasswordServiceTest {
    private final GetUserByEmailPort getUserByEmailPort = Mockito.mock(GetUserByEmailPort.class);
    private final GenerateChangePasswordTokenUseCase generateChangePasswordTokenUseCase = Mockito.mock(GenerateChangePasswordTokenUseCase.class);
    private final SendResetPasswordEmailPort sendResetPasswordEmailPort = Mockito.mock(SendResetPasswordEmailPort.class);

    private final ForgotPasswordService forgotPasswordService = new ForgotPasswordService(getUserByEmailPort, generateChangePasswordTokenUseCase, sendResetPasswordEmailPort);

    @Test
    void forgotPasswordEmpty(){
        when(getUserByEmailPort.getUserByEmail(any(String.class))).thenReturn(Optional.empty());
        forgotPasswordService.forgotPassword("test2@gmail.com");
    }

    @Test
    void forgotPasswordSuccess(){
        var user = new User(1L, UUID.randomUUID(), "Jhair", "Guzman", "Tito");
        when(getUserByEmailPort.getUserByEmail(any(String.class))).thenReturn(Optional.of(user));
        when(generateChangePasswordTokenUseCase.generateChangePasswordToken(any(String.class), any(String.class))).thenReturn("token");
        doNothing().when(sendResetPasswordEmailPort).sendResetPasswordEmailPort(user, "test2@gmail.com","token");
        forgotPasswordService.forgotPassword("test2@gmail.com");
    }
}
