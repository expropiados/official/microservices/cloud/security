package org.expropiados.cloudservices.security.modules.login.application;

import org.expropiados.cloudservices.security.helpers.PasswordHelper;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetAllActiveUserRolesByEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetHashedPasswordPort;
import org.expropiados.cloudservices.security.modules.login.domain.AccessCredentials;
import org.expropiados.cloudservices.security.modules.login.domain.roles.AllRoles;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateAccessTokenUseCase;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class LoginServiceTest {
    private final PasswordHelper passwordHelper = Mockito.mock(PasswordHelper.class);
    private final GetHashedPasswordPort getHashedPasswordPort = Mockito.mock(GetHashedPasswordPort.class);
    private final GenerateAccessTokenUseCase generateAccessTokenUseCase = Mockito.mock(GenerateAccessTokenUseCase.class);
    private final GetAllActiveUserRolesByEmailPort getAllActiveUserRolesByEmailPort = Mockito.mock(GetAllActiveUserRolesByEmailPort.class);

    private final LoginService loginService = new LoginService(passwordHelper, getHashedPasswordPort, generateAccessTokenUseCase, getAllActiveUserRolesByEmailPort);

    @Test
    void loginServiceFailure(){
        var accessCredentials = new AccessCredentials("test2@gmail.com", "token");
        var allRoles = AllRoles.builder().build();
        when(getHashedPasswordPort.getHashedPassword("test2@gmail.com")).thenReturn(Optional.of("token"));
        when(getAllActiveUserRolesByEmailPort.getAllActiveUserRolesByEmail("test2@gmail.com")).thenReturn(allRoles);
        when(generateAccessTokenUseCase.generateAccessToken(allRoles, "test2@gmail.com")).thenReturn("token2");
        try{
            var result = loginService.login(accessCredentials);
        }catch(LoginNotAllowedForCredentialsException e){
            assertThat(e.getMessage()).isEqualTo("Wrong password for user with email 'test2@gmail.com'");
        }
    }
}
