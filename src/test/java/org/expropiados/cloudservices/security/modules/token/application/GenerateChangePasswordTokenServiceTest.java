package org.expropiados.cloudservices.security.modules.token.application;

import org.expropiados.cloudservices.security.config.security.JwtConfig;
import org.expropiados.cloudservices.security.helpers.JwtHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GenerateChangePasswordTokenServiceTest {
    private final JwtConfig.Login login = Mockito.mock(JwtConfig.Login.class);
    private final JwtConfig.ResetPassword resetPassword = Mockito.mock(JwtConfig.ResetPassword.class);
    private final JwtHelper jwtHelper = Mockito.mock(JwtHelper.class);
    private final JwtConfig jwtConfig =  new JwtConfig();
    private GenerateChangePasswordTokenService generateAccessTokenService = null;

    @BeforeEach
    void init(){
        jwtConfig.setLogin(login);
        jwtConfig.setResetPassword(resetPassword);
        generateAccessTokenService = new GenerateChangePasswordTokenService(jwtHelper, jwtConfig);
    }

    @Test
    void generateAccessTokenException(){
        try{
            var result = generateAccessTokenService.generateChangePasswordToken(null, null);
        }catch (NullPointerException e){
            e.getCause();
        }
    }

    @Test
    void generateAccessTokenSuccess(){
        when(jwtConfig.getResetPassword().getExpirationInMinutes()).thenReturn(60L);
        when(jwtHelper.generateToken(any(String.class), any(Map.class), eq(60L))).thenReturn("ResetPassword");

        var result = generateAccessTokenService.generateChangePasswordToken("contra", "test2@gmail.com");

        assertThat(result).isEqualTo("ResetPassword");
    }
}
