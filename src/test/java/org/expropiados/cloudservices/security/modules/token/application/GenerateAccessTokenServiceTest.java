package org.expropiados.cloudservices.security.modules.token.application;

import org.expropiados.cloudservices.security.config.security.JwtConfig;
import org.expropiados.cloudservices.security.helpers.JwtHelper;
import org.expropiados.cloudservices.security.modules.login.domain.roles.Admin;
import org.expropiados.cloudservices.security.modules.login.domain.roles.AllRoles;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import java.util.Map;
import java.util.Optional;

@Tag("UnitTest")
class GenerateAccessTokenServiceTest {
    private final JwtConfig.Login login = Mockito.mock(JwtConfig.Login.class);
    private final JwtConfig.ResetPassword resetPassword = Mockito.mock(JwtConfig.ResetPassword.class);
    private final JwtHelper jwtHelper = Mockito.mock(JwtHelper.class);
    private final JwtConfig jwtConfig =  new JwtConfig();
    private GenerateAccessTokenService generateAccessTokenService = null;

    @BeforeEach
    void init(){
        jwtConfig.setLogin(login);
        jwtConfig.setResetPassword(resetPassword);

        generateAccessTokenService = new GenerateAccessTokenService(jwtHelper, jwtConfig);
    }

    @Test
    void generateAccessTokenException(){
        try{
            var result = generateAccessTokenService.generateAccessToken(null, null);
        }catch (NullPointerException e){
            e.getCause();
        }
    }

    @Test
    void generateAccessTokenSuccess(){
        var admin = new Admin("Jhair Guzman", "test2@gmail.com", "expropiados");
        var allRoles = AllRoles.builder().admin(admin).clientSupervisor(Optional.empty())
                .patient(Optional.empty()).providerSupervisor(Optional.empty()).specialist(Optional.empty()).build();

        when(jwtConfig.getLogin().getExpirationInMinutes()).thenReturn(60L);
        when(jwtHelper.generateToken(any(String.class), any(Map.class), eq(60L))).thenReturn("Login");

        var result = generateAccessTokenService.generateAccessToken(allRoles, "test2@gmail.com");

        assertThat(result).isEqualTo("Login");
    }
}
