package org.expropiados.cloudservices.security.modules.login.application;

import org.expropiados.cloudservices.security.helpers.PasswordHelper;
import org.expropiados.cloudservices.security.modules.login.application.port.out.StorePasswordPort;
import org.expropiados.cloudservices.security.modules.token.application.port.in.DecodeChangePasswordTokenUseCase;
import org.expropiados.cloudservices.security.modules.token.domain.ChangePasswordTokenBody;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Tag("UnitTest")
class ChangePasswordServiceTest {
    private final DecodeChangePasswordTokenUseCase changePasswordTokenUseCase = Mockito.mock(DecodeChangePasswordTokenUseCase.class);
    private final StorePasswordPort storePasswordPort = Mockito.mock(StorePasswordPort.class);
    private final PasswordHelper passwordHelper = Mockito.mock(PasswordHelper.class);
    private final ChangePasswordService changePasswordService = new ChangePasswordService(changePasswordTokenUseCase, storePasswordPort, passwordHelper);

    @Test
    void changePasswordFailure(){
        when(changePasswordTokenUseCase.decodeChangePasswordToken(any(String.class))).thenReturn(null);
        try{
            changePasswordService.changePassword("token", "password");
        }catch(NullPointerException e){
            e.getCause();
        }
    }

    @Test
    void changePasswordSuccess(){
        var changePassword = new ChangePasswordTokenBody("test2@gmail.com", "test2");
        when(changePasswordTokenUseCase.decodeChangePasswordToken(any(String.class))).thenReturn(changePassword);
        when(passwordHelper.generateHashedPassword("test2")).thenReturn("ABCD");
        doNothing().when(storePasswordPort).storePassword("test2@gmail.com", "ABCD");

        changePasswordService.changePassword("token", "test2");
    }
}
