package org.expropiados.cloudservices.security.modules.login.application;

import org.expropiados.cloudservices.security.modules.login.application.port.out.*;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateChangePasswordTokenUseCase;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class RegisterRoleEmailServiceTest {
    private final GetCurrentRolesForUserEmailPort getCurrentRolesForUserEmailPort = Mockito.mock(GetCurrentRolesForUserEmailPort.class);
    private final GetUserPort getUserPort = Mockito.mock(GetUserPort.class);
    private final RegisterEmailPort registerEmailPort = Mockito.mock(RegisterEmailPort.class);
    private final SendWelcomeAndChangePasswordEmailPort sendWelcomeAndChangePasswordEmailPort = Mockito.mock(SendWelcomeAndChangePasswordEmailPort.class);
    private final SendRoleAddedEmailPort sendRoleAddedEmailPort = Mockito.mock(SendRoleAddedEmailPort.class);
    private final GenerateChangePasswordTokenUseCase generateChangePasswordUseCase = Mockito.mock(GenerateChangePasswordTokenUseCase.class);

    private final RegisterRoleEmailService registerRoleEmailService = new RegisterRoleEmailService(getCurrentRolesForUserEmailPort, getUserPort, registerEmailPort, sendWelcomeAndChangePasswordEmailPort, sendRoleAddedEmailPort, generateChangePasswordUseCase);

    @Test
    void registerRoleException(){
        var role = UserRole.ADMIN;
        var roles = new ArrayList<UserRole>();
        roles.add(role);
        when(getCurrentRolesForUserEmailPort.getCurrentRolesForUserEmail("test2@gmail.com")).thenReturn(roles);
        try{
            registerRoleEmailService.registerRoleEmail("test2@gmail.com", UserRole.CLIENT_SUPERVISOR);

        }catch (RoleNotFoundForUserException e){
            assertThat(e.getMessage()).isEqualTo("User with email 'test2@gmail.com' has no role 'CLIENT_SUPERVISOR'");
        }
    }
}
