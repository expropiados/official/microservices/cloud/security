package org.expropiados.cloudservices.security.config.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "security.sendgrid")
public class SendGridConfig {
    private String apiKey;
    private String senderEmail;
    private Templates templates;
    private Variables variables;

    @Getter @Setter
    public static class Templates {
        private String welcomeId;
        private String roleAddedId;
        private String resetPasswordId;
    }

    @Getter @Setter
    public static class Variables {
        private String confirmAccountUrl;
        private String resetPasswordUrl;
        private String frontendHost;
    }
}
