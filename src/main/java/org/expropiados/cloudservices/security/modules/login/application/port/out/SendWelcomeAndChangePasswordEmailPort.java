package org.expropiados.cloudservices.security.modules.login.application.port.out;

import org.expropiados.cloudservices.security.modules.login.domain.roles.User;

public interface SendWelcomeAndChangePasswordEmailPort {
    void sendWelcomeAndChangePasswordEmail(User user, String email, String changePasswordToken);
}
