package org.expropiados.cloudservices.security.modules.login.application.port.out;

import java.util.Optional;

public interface GetHashedPasswordPort {
    Optional<String> getHashedPassword(String email);
}
