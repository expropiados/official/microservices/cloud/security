package org.expropiados.cloudservices.security.modules.login.application.port.in;

import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

public interface RegisterUseCase {
    void registerRoleEmail(String email, UserRole role);
}
