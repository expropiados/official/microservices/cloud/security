package org.expropiados.cloudservices.security.modules.login.domain.roles;

import lombok.Value;

import java.util.UUID;

@Value
public class User {
    Long id;
    UUID uuid;
    String names;
    String fatherLastname;
    String motherLastname;

    public String fullnames() {
        return String.format("%s %s %s", names, fatherLastname, motherLastname);
    }
}
