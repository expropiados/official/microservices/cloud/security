package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.expropiados.cloudservices.security.modules.login.domain.roles.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "names", source = "name")
    @Mapping(target = "fatherLastname", source = "fatherLastname")
    @Mapping(target = "motherLastname", source = "motherLastname")
    User toUser(UserJpaEntity userJpaEntity);
}
