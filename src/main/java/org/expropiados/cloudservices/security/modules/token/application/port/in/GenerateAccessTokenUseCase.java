package org.expropiados.cloudservices.security.modules.token.application.port.in;

import org.expropiados.cloudservices.security.modules.login.domain.roles.AllRoles;

public interface GenerateAccessTokenUseCase {
    String generateAccessToken(AllRoles roles, String email);
}
