package org.expropiados.cloudservices.security.modules.login.domain.roles;

import lombok.Value;

import java.util.UUID;

@Value
public class ProviderSupervisor {
    Long id;
    User user;
    Boolean isBlocked;
}
