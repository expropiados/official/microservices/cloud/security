package org.expropiados.cloudservices.security.modules.login.domain;

import lombok.Value;

@Value
public class UserTokens {
    String accessToken;
    String refreshToken;
}
