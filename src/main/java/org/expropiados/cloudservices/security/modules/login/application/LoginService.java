package org.expropiados.cloudservices.security.modules.login.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.helpers.PasswordHelper;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.in.LoginUseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetAllActiveUserRolesByEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetHashedPasswordPort;
import org.expropiados.cloudservices.security.modules.login.domain.AccessCredentials;
import org.expropiados.cloudservices.security.modules.login.domain.UserTokens;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateAccessTokenUseCase;

@UseCase
@RequiredArgsConstructor
public class LoginService implements LoginUseCase {

    private final PasswordHelper passwordHelper;
    private final GetHashedPasswordPort getHashedPasswordPort;
    private final GenerateAccessTokenUseCase generateAccessTokenUseCase;
    private final GetAllActiveUserRolesByEmailPort getAllActiveUserRolesByEmailPort;

    @Override
    public UserTokens login(AccessCredentials credentials) {
        var password = credentials.getPassword();
        var email = credentials.getEmail();

        var hashedPassword = getHashedPasswordPort
                .getHashedPassword(email)
                .orElseThrow(() -> new UserNotRegisteredException(email));

        if (!passwordHelper.areSamePasswords(password, hashedPassword)) {
            throw new LoginNotAllowedForCredentialsException(email);
        }

        var allRoles = getAllActiveUserRolesByEmailPort.getAllActiveUserRolesByEmail(email);
        var accessToken = generateAccessTokenUseCase.generateAccessToken(allRoles, email);
        return new UserTokens(accessToken, accessToken);
    }
}
