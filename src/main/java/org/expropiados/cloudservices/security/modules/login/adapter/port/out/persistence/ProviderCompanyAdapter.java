package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.config.backoffice.ProviderCompanyConfig;
import org.expropiados.cloudservices.security.hexagonal.PersistenceAdapter;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.ProviderCompanyJpaEntity;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.ProviderCompanyJpaRepository;
import org.expropiados.cloudservices.security.modules.login.domain.roles.Admin;

@PersistenceAdapter
@RequiredArgsConstructor
public class ProviderCompanyAdapter {
    private final ProviderCompanyJpaRepository providerCompanyJpaRepository;
    private final ProviderCompanyConfig providerCompanyConfig;

    public String getProviderCompanyName() {
        var uuid = providerCompanyConfig.getUuid();
        var company = providerCompanyJpaRepository.findByUuid(uuid);

        return company.map(ProviderCompanyJpaEntity::getBusinessName).orElse("");
    }

    public Admin getAdminFromCompany() {
        var uuid = providerCompanyConfig.getUuid();
        var optCompany = providerCompanyJpaRepository.findByUuid(uuid);

        if (optCompany.isEmpty()) {
            return null;
        }

        var company = optCompany.get();

        return new Admin(
                company.getResponsibleName(),
                company.getResponsibleEmail(),
                company.getBusinessName()
        );
    }
}
