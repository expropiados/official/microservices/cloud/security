package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.WebAdapter;
import org.expropiados.cloudservices.security.modules.login.application.port.in.RegisterUseCase;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class RegisterRoleEmailController {
    private final RegisterUseCase useCase;

    @PostMapping("/register")
    public void registerRoleEmail(@RequestBody RegisterRoleEmailInput registerRoleEmailInput) {
        useCase.registerRoleEmail(registerRoleEmailInput.getEmail(), registerRoleEmailInput.getRole());
    }
}
