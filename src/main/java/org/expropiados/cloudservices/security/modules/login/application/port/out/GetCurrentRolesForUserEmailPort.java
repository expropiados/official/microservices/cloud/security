package org.expropiados.cloudservices.security.modules.login.application.port.out;

import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import java.util.List;

public interface GetCurrentRolesForUserEmailPort {
    List<UserRole> getCurrentRolesForUserEmail(String email);
}
