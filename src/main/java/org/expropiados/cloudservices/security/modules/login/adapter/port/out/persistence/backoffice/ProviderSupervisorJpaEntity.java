package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import lombok.Data;

import javax.persistence.*;

@Data @Entity
@Table(name = "PROVIDER_SUPERVISOR", schema = "backoffice")
public class ProviderSupervisorJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "provider_supervisor_id")
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name = "is_blocked")
    private Boolean isBlocked;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserJpaEntity user;
}
