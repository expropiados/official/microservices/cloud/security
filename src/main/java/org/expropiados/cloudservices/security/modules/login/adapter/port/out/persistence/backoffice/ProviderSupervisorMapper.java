package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.expropiados.cloudservices.security.modules.login.domain.roles.Patient;
import org.expropiados.cloudservices.security.modules.login.domain.roles.ProviderSupervisor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface ProviderSupervisorMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "isBlocked", source = "isBlocked")
    ProviderSupervisor toProviderSupervisor(ProviderSupervisorJpaEntity providerSupervisorJpaEntity);
}
