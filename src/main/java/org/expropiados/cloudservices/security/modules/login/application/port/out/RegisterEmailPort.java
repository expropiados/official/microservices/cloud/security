package org.expropiados.cloudservices.security.modules.login.application.port.out;

public interface RegisterEmailPort {
    void registerEmail(String email);
}
