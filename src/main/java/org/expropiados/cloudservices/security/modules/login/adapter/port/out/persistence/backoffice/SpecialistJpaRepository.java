package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SpecialistJpaRepository extends JpaRepository<SpecialistJpaEntity, Long> {
    Boolean existsSpecialistJpaEntityByEmail(String email);
    Optional<SpecialistJpaEntity> findByEmail(String email);
    Optional<SpecialistJpaEntity> findByEmailAndIsBlocked(String email, Boolean isBlocked);;
}
