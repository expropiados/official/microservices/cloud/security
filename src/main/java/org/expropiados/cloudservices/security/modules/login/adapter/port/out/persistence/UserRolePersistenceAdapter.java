package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.PersistenceAdapter;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.ClientSupervisorJpaRepository;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.PatientJpaRepository;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.ProviderSupervisorJpaRepository;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.SpecialistJpaRepository;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetCurrentRolesForUserEmailPort;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import java.util.ArrayList;
import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserRolePersistenceAdapter implements GetCurrentRolesForUserEmailPort {

    private final PatientJpaRepository patientJpaRepository;
    private final SpecialistJpaRepository specialistJpaRepository;
    private final ProviderSupervisorJpaRepository providerSupervisorJpaRepository;
    private final ClientSupervisorJpaRepository clientSupervisorJpaRepository;

    @Override
    public List<UserRole> getCurrentRolesForUserEmail(String email) {
        var existsPatient = patientJpaRepository.existsPatientJpaEntityByEmail(email);
        var existsSpecialist = specialistJpaRepository.existsSpecialistJpaEntityByEmail(email);
        var existsProvSupervisor = providerSupervisorJpaRepository.existsProviderSupervisorJpaEntityByEmail(email);
        var existsClientSupervisor = clientSupervisorJpaRepository.existsClientSupervisorJpaEntityByEmail(email);

        var currentRoles = new ArrayList<UserRole>();

        if (existsPatient)              currentRoles.add(UserRole.PATIENT);
        if (existsSpecialist)           currentRoles.add(UserRole.SPECIALIST);
        if (existsProvSupervisor)       currentRoles.add(UserRole.PROVIDER_SUPERVISOR);
        if (existsClientSupervisor)     currentRoles.add(UserRole.CLIENT_SUPERVISOR);

        return currentRoles;
    }
}
