package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.WebAdapter;
import org.expropiados.cloudservices.security.modules.login.application.port.in.ChangePasswordUseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.in.ForgotPasswordUseCase;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ForgotPasswordController {
    private final ForgotPasswordUseCase useCase;

    @PostMapping("/credentials/password")
    public void forgotPassword(@RequestBody ForgotPasswordInput forgotPasswordInput) {
        useCase.forgotPassword(forgotPasswordInput.getEmail());
    }
}
