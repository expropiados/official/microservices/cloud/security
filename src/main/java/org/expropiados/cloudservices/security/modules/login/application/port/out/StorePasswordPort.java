package org.expropiados.cloudservices.security.modules.login.application.port.out;

public interface StorePasswordPort {
    void storePassword(String email, String hashedPassword);
}
