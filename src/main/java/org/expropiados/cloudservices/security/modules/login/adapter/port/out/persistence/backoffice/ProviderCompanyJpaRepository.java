package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProviderCompanyJpaRepository extends JpaRepository<ProviderCompanyJpaEntity, Long> {
    Optional<ProviderCompanyJpaEntity> findByUuid(UUID uuid);
}
