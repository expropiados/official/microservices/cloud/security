package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.Value;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
public class ChangePasswordInput {

    @NotBlank
    String newPassword;

    @NotBlank
    String token;
}
