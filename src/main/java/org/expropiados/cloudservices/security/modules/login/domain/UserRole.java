package org.expropiados.cloudservices.security.modules.login.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserRole {
    SPECIALIST("Especialista"),
    PATIENT("Paciente"),
    CLIENT_SUPERVISOR("Supervisor"),
    PROVIDER_SUPERVISOR("Supervisor"),
    ADMIN("Administrador");

    @Getter
    private final String value;
}
