package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.WebAdapter;
import org.expropiados.cloudservices.security.modules.login.application.port.in.ChangePasswordUseCase;
import org.expropiados.cloudservices.security.modules.login.domain.AccessCredentials;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ChangePasswordController {
    private final ChangePasswordUseCase useCase;

    @PutMapping("/credentials/password")
    public void changePassword(@RequestBody ChangePasswordInput input) {
        useCase.changePassword(input.getToken(), input.getNewPassword());
    }
}
