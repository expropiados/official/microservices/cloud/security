package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProviderSupervisorJpaRepository extends JpaRepository<ProviderSupervisorJpaEntity, Long> {
    Boolean existsProviderSupervisorJpaEntityByEmail(String email);
    Optional<ProviderSupervisorJpaEntity> findByEmail(String email);
    Optional<ProviderSupervisorJpaEntity> findByEmailAndIsBlocked(String email, Boolean isBlocked);;
}
