package org.expropiados.cloudservices.security.modules.token.application;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cloudservices.security.hexagonal.errors.NotFoundException;
import org.expropiados.cloudservices.security.hexagonal.errors.UserInputException;

import java.io.Serializable;

@Getter
public class InvalidTokenException extends RuntimeException implements UserInputException {
    private final String code = "SEC_TKN_001";
    private final String message;
    private final Object data;

    @Value
    static class Data implements Serializable {
    }

    public InvalidTokenException() {
        super();
        this.message = "Token sent is invalid";
        this.data = new InvalidTokenException.Data();
    }
}
