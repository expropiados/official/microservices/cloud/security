package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "CLIENT_COMPANY", schema = "backoffice")
public class CompanyJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_company_id")
    private Long id;

    @Column(name = "business_name", length = 150, nullable = false)
    private String name;
}
