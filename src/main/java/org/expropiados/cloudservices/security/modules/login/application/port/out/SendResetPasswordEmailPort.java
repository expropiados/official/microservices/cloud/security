package org.expropiados.cloudservices.security.modules.login.application.port.out;

import org.expropiados.cloudservices.security.modules.login.domain.roles.User;

public interface SendResetPasswordEmailPort {
    void sendResetPasswordEmailPort(User user, String email, String token);
}
