package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserCredentialJpaRepository extends JpaRepository<UserCredentialJpaEntity, Long> {
    Optional<UserCredentialJpaEntity> findByEmail(String email);
}
