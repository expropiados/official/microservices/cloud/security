package org.expropiados.cloudservices.security.modules.token.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.config.security.JwtConfig;
import org.expropiados.cloudservices.security.helpers.JwtHelper;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateChangePasswordTokenUseCase;
import org.expropiados.cloudservices.security.modules.token.domain.ChangePasswordTokenBody;

@UseCase
@RequiredArgsConstructor
public class GenerateChangePasswordTokenService implements GenerateChangePasswordTokenUseCase {

    private final JwtHelper jwtHelper;
    private final JwtConfig jwtConfig;

    @Override
    public String generateChangePasswordToken(String username, String email) {
        var expirationTimeInMinutes = jwtConfig.getResetPassword().getExpirationInMinutes();

        var claims = new ChangePasswordTokenBody(email, username).map();
        return jwtHelper.generateToken(username, claims, expirationTimeInMinutes);
    }
}
