package org.expropiados.cloudservices.security.modules.token.domain;

import lombok.Builder;
import lombok.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Value
@Builder
public class AccessTokenBody {

    boolean isAdmin;
    String username;
    String email;
    String userImageUrl;

    UUID userUuid;
    UUID patientUuid;
    UUID specialistUuid;
    UUID clientSupervisorUuid;

    Long userId;
    Long patientId;
    Long specialistId;
    Long clientSupervisorId;
    Long providerSupervisorId;

    Long patientCompanyId;
    String patientCompanyName;

    Long clientSupervisorCompanyId;
    String clientSupervisorCompanyName;

    String providerSupervisorCompanyName;

    public Map<String, Object> map() {
        var claims = new HashMap<String, Object>();

        claims.put("us_uid", userUuid);
        claims.put("pa_uid", patientUuid);
        claims.put("sp_uid", specialistUuid);
        claims.put("cs_uid", clientSupervisorUuid);

        claims.put("pa_com_id", patientCompanyId);
        claims.put("cs_com_id", clientSupervisorCompanyId);

        claims.put("us_id", userId);
        claims.put("pa_id", patientId);
        claims.put("sp_id", specialistId);
        claims.put("cs_id", clientSupervisorId);
        claims.put("ps_id", providerSupervisorId);

        claims.put("pa_com", patientCompanyName);
        claims.put("cs_com", clientSupervisorCompanyName);
        claims.put("ps_com", providerSupervisorCompanyName);

        claims.put("admin", isAdmin);
        claims.put("email", email);
        claims.put("img", userImageUrl);

        return claims;
    }
}
