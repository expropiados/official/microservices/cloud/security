package org.expropiados.cloudservices.security.modules.login.domain.roles;

import lombok.Value;

import java.util.UUID;

@Value
public class Specialist {
    Long id;
    UUID uuid;
    User user;
    Boolean isBlocked;
}
