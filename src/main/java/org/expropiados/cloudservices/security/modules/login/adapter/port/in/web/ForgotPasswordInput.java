package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.Data;

@Data
public class ForgotPasswordInput {
    private String email;
}
