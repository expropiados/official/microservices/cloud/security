package org.expropiados.cloudservices.security.modules.login.domain;

import lombok.Value;

@Value
public class AccessCredentials {
    String email;
    String password;
}
