package org.expropiados.cloudservices.security.modules.login.application.port.out;

public interface RegisterCredentialsPort {
    void registerCredentialsPort(String hashedPassword, String email);
}
