package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.expropiados.cloudservices.security.modules.login.domain.roles.Specialist;
import org.expropiados.cloudservices.security.modules.login.domain.roles.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface SpecialistMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "isBlocked", source = "isBlocked")
    Specialist toSpecialist(SpecialistJpaEntity specialistJpaEntity);
}
