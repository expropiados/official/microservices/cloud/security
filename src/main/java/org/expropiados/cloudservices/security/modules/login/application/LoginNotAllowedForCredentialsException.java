package org.expropiados.cloudservices.security.modules.login.application;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cloudservices.security.hexagonal.errors.NotFoundException;
import org.expropiados.cloudservices.security.hexagonal.errors.UserInputException;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import java.io.Serializable;

@Getter
public class LoginNotAllowedForCredentialsException extends RuntimeException implements UserInputException {
    private final String code = "SEC_LOG_003";
    private final String message;
    private final Object data;

    @Value
    static class Data implements Serializable {
        String email;
    }

    public LoginNotAllowedForCredentialsException(String email) {
        super();
        this.message = String.format("Wrong password for user with email '%s'", email);
        this.data = new LoginNotAllowedForCredentialsException.Data(email);
    }
}
