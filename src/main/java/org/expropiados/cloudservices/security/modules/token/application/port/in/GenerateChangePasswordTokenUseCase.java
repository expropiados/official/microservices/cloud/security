package org.expropiados.cloudservices.security.modules.token.application.port.in;

public interface GenerateChangePasswordTokenUseCase {
    String generateChangePasswordToken(String username, String email);
}
