package org.expropiados.cloudservices.security.modules.login.application.port.in;

public interface ForgotPasswordUseCase {
    void forgotPassword(String email);
}
