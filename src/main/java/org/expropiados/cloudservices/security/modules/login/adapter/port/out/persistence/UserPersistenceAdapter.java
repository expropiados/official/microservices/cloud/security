package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.PersistenceAdapter;
import org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice.*;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetAllActiveUserRolesByEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetUserByEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetUserPort;
import org.expropiados.cloudservices.security.modules.login.domain.roles.AllRoles;
import org.expropiados.cloudservices.security.modules.login.domain.roles.User;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserPersistenceAdapter implements GetUserPort, GetAllActiveUserRolesByEmailPort, GetUserByEmailPort {

    private final PatientJpaRepository patientJpaRepository;
    private final SpecialistJpaRepository specialistJpaRepository;
    private final ProviderSupervisorJpaRepository providerSupervisorJpaRepository;
    private final ClientSupervisorJpaRepository clientSupervisorJpaRepository;

    private final UserMapper userMapper;
    private final PatientMapper patientMapper;
    private final SpecialistMapper specialistMapper;
    private final ClientSupervisorMapper clientSupervisorMapper;
    private final ProviderSupervisorMapper providerSupervisorMapper;

    private final ProviderCompanyAdapter providerCompanyAdapter;

    @Override
    public User getUser(String email, UserRole role) {
        if (role == UserRole.PATIENT) {
            var patient = patientJpaRepository.findByEmail(email).get();
            return userMapper.toUser(patient.getUser());
        }

        if (role == UserRole.SPECIALIST) {
            var specialist = specialistJpaRepository.findByEmail(email).get();
            return userMapper.toUser(specialist.getUser());
        }

        if (role == UserRole.PROVIDER_SUPERVISOR) {
            var supervisor = providerSupervisorJpaRepository.findByEmail(email).get();
            return userMapper.toUser(supervisor.getUser());
        }

        // CLIENT SUPERVISOR
        var supervisor = clientSupervisorJpaRepository.findByEmail(email).get();
        return userMapper.toUser(supervisor.getUser());
    }

    @Override
    public AllRoles getAllActiveUserRolesByEmail(String email) {
        var providerCompanyName = providerCompanyAdapter.getProviderCompanyName();
        var admin = providerCompanyAdapter.getAdminFromCompany();

        var patient = patientJpaRepository
                .findByEmailAndIsBlocked(email, false)
                .map(patientMapper::toPatient);

        var specialist = specialistJpaRepository
                .findByEmailAndIsBlocked(email, false)
                .map(specialistMapper::toSpecialist);

        var clientSupervisor = clientSupervisorJpaRepository
                .findByEmailAndIsBlocked(email, false)
                .map(clientSupervisorMapper::toClientSupervisor);

        var providerSupervisor = providerSupervisorJpaRepository
                .findByEmailAndIsBlocked(email, false)
                .map(providerSupervisorMapper::toProviderSupervisor);

        return AllRoles.builder()
                .admin(admin)
                .providerCompanyName(providerCompanyName)
                .patient(patient)
                .specialist(specialist)
                .clientSupervisor(clientSupervisor)
                .providerSupervisor(providerSupervisor)
                .build();
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        var patient = patientJpaRepository.findByEmail(email);
        if (patient.isPresent()) {
            var user = userMapper.toUser(patient.get().getUser());
            return Optional.of(user);
        }

        var specialist = specialistJpaRepository.findByEmail(email);
        if (specialist.isPresent()) {
            var user = userMapper.toUser(specialist.get().getUser());
            return Optional.of(user);
        }

        var providerSupervisor = providerSupervisorJpaRepository.findByEmail(email);
        if (providerSupervisor.isPresent()) {
            var user = userMapper.toUser(providerSupervisor.get().getUser());
            return Optional.of(user);
        }

        var clientSupervisor = providerSupervisorJpaRepository.findByEmail(email);
        if (clientSupervisor.isPresent()) {
            var user = userMapper.toUser(clientSupervisor.get().getUser());
            return Optional.of(user);
        }

        return Optional.empty();
    }
}
