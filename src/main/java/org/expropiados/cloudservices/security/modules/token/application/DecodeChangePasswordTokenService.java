package org.expropiados.cloudservices.security.modules.token.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.helpers.JwtHelper;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.token.application.port.in.DecodeChangePasswordTokenUseCase;
import org.expropiados.cloudservices.security.modules.token.domain.ChangePasswordTokenBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@UseCase
@RequiredArgsConstructor
public class DecodeChangePasswordTokenService implements DecodeChangePasswordTokenUseCase {

    private final Logger logger = LoggerFactory.getLogger(DecodeChangePasswordTokenService.class);
    private final JwtHelper jwtHelper;

    @Override
    public ChangePasswordTokenBody decodeChangePasswordToken(String token) {
        try {
            var claims = jwtHelper.decode(token);
            var email = claims.get("email", String.class);
            var username = claims.getSubject();

            return new ChangePasswordTokenBody(email, username);

        } catch (Exception exception) {
            logger.error(exception.getMessage(), exception);
            throw new InvalidTokenException();
        }
    }
}
