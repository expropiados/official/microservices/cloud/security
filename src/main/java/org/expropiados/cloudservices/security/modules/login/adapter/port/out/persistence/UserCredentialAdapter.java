package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.PersistenceAdapter;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetHashedPasswordPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.RegisterEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.StorePasswordPort;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserCredentialAdapter implements RegisterEmailPort, GetHashedPasswordPort, StorePasswordPort {

    private final UserCredentialJpaRepository userCredentialJpaRepository;

    @Override
    public void registerEmail(String email) {
        var row = userCredentialJpaRepository
                .findByEmail(email)
                .orElseGet(UserCredentialJpaEntity::new);

        row.setEmail(email);
        userCredentialJpaRepository.save(row);
    }

    @Override
    public Optional<String> getHashedPassword(String email) {
        var row = userCredentialJpaRepository.findByEmail(email);
        return row.map(UserCredentialJpaEntity::getHashedPassword);
    }

    @Override
    public void storePassword(String email, String hashedPassword) {
        var row = userCredentialJpaRepository
                .findByEmail(email)
                .orElseGet(UserCredentialJpaEntity::new);

        row.setEmail(email);
        row.setHashedPassword(hashedPassword);
        userCredentialJpaRepository.save(row);
    }
}
