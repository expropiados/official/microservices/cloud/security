package org.expropiados.cloudservices.security.modules.token.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.config.security.JwtConfig;
import org.expropiados.cloudservices.security.helpers.JwtHelper;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.login.domain.roles.AllRoles;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateAccessTokenUseCase;
import org.expropiados.cloudservices.security.modules.token.domain.AccessTokenBody;

@UseCase
@RequiredArgsConstructor
public class GenerateAccessTokenService implements GenerateAccessTokenUseCase {

    private final JwtHelper jwtHelper;
    private final JwtConfig jwtConfig;

    @Override
    public String generateAccessToken(AllRoles roles, String email) {
        var admin = roles.getAdmin();
        var patient = roles.getPatient();
        var specialist = roles.getSpecialist();
        var clientSupervisor = roles.getClientSupervisor();
        var providerSupervisor = roles.getProviderSupervisor();

        var tokenBuilder = AccessTokenBody
                .builder()
                .providerSupervisorCompanyName(roles.getProviderCompanyName())
                .email(email);

        var expirationTimeInMinutes = jwtConfig.getLogin().getExpirationInMinutes();

        if (admin != null && admin.getEmail().equals(email)) {
            tokenBuilder
                    .isAdmin(true)
                    .username(admin.getFullName())
                    .providerSupervisorCompanyName(admin.getCompanyName());
        }

        if (patient.isPresent()) {
            var p = patient.get();
            tokenBuilder
                    .username(p.getUser().fullnames())
                    .userId(p.getUser().getId())
                    .userUuid(p.getUser().getUuid())
                    .patientId(p.getId())
                    .patientUuid(p.getUuid())
                    .patientCompanyId(p.getCompanyId())
                    .patientCompanyName(p.getCompanyName());
        }

        if (specialist.isPresent()) {
            var s = specialist.get();
            tokenBuilder
                    .username(s.getUser().fullnames())
                    .userId(s.getUser().getId())
                    .userUuid(s.getUser().getUuid())
                    .specialistId(s.getId())
                    .specialistUuid(s.getUuid());
        }

        if (clientSupervisor.isPresent()) {
            var cs = clientSupervisor.get();
            tokenBuilder
                    .username(cs.getUser().fullnames())
                    .userId(cs.getUser().getId())
                    .userUuid(cs.getUser().getUuid())
                    .clientSupervisorId(cs.getId())
                    .clientSupervisorUuid(cs.getUuid())
                    .clientSupervisorCompanyId(cs.getCompanyId())
                    .clientSupervisorCompanyName(cs.getCompanyName());
        }

        if (providerSupervisor.isPresent()) {
            var ps = providerSupervisor.get();
            tokenBuilder
                    .username(ps.getUser().fullnames())
                    .userId(ps.getUser().getId())
                    .userUuid(ps.getUser().getUuid())
                    .providerSupervisorId(ps.getId());
        }

        var tokenBody = tokenBuilder.build();
        return jwtHelper.generateToken(tokenBody.getUsername(), tokenBody.map(), expirationTimeInMinutes);
    }
}
