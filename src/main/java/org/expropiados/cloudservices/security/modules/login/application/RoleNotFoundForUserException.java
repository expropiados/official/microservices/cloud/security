package org.expropiados.cloudservices.security.modules.login.application;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cloudservices.security.hexagonal.errors.NotFoundException;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import java.io.Serializable;

@Getter
public class RoleNotFoundForUserException extends RuntimeException implements NotFoundException {
    private final String code = "SEC_LOG_001";
    private final String message;
    private final Object data;

    @Value
    static class Data implements Serializable {
        String email;
        UserRole userRole;
    }

    public RoleNotFoundForUserException(String email, UserRole role) {
        super();
        this.message = String.format("User with email '%s' has no role '%s'", email, role);
        this.data = new RoleNotFoundForUserException.Data(email, role);
    }
}
