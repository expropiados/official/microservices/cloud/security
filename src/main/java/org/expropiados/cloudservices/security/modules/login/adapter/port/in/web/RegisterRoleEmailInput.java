package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.Value;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

import javax.validation.constraints.*;

@Value
public class RegisterRoleEmailInput {

    @Email @NotBlank
    String email;

    @NotNull
    UserRole role;
}
