package org.expropiados.cloudservices.security.modules.login.domain.roles;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Admin {
    private String fullName;
    private String email;
    private String companyName;
}
