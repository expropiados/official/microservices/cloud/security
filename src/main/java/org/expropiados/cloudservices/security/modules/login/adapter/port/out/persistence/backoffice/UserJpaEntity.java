package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "GENERAL_USER", schema = "backoffice")
public class UserJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name ="user_uuid", nullable = false)
    private UUID uuid;

    @Column(name ="name", length = 100, nullable = false)
    private String name;

    @Column(name ="father_lastname", length = 100, nullable = false)
    private String fatherLastname;

    @Column(name ="mother_lastname", length = 100, nullable = false)
    private String motherLastname;
}
