package org.expropiados.cloudservices.security.modules.login.application.port.out;

import org.expropiados.cloudservices.security.modules.login.domain.roles.AllRoles;

public interface GetAllActiveUserRolesByEmailPort {
    AllRoles getAllActiveUserRolesByEmail(String email);
}
