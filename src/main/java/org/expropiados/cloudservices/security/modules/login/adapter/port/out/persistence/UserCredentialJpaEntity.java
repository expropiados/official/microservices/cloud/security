package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "USER_CREDENTIAL")
public class UserCredentialJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_credential_id")
    private Long id;

    @Column(name = "email", unique = true, nullable = false, length = 50)
    private String email;

    @Column(name = "hashed_password", length = 300)
    private String hashedPassword;
}
