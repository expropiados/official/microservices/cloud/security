package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientSupervisorJpaRepository extends JpaRepository<ClientSupervisorJpaEntity, Long> {
    Boolean existsClientSupervisorJpaEntityByEmail(String email);
    Optional<ClientSupervisorJpaEntity> findByEmail(String email);
    Optional<ClientSupervisorJpaEntity> findByEmailAndIsBlocked(String email, Boolean isBlocked);
}
