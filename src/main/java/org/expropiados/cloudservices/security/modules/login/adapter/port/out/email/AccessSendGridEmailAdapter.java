package org.expropiados.cloudservices.security.modules.login.adapter.port.out.email;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.config.security.SendGridConfig;
import org.expropiados.cloudservices.security.hexagonal.EmailAdapter;
import org.expropiados.cloudservices.security.modules.login.application.port.out.SendResetPasswordEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.SendRoleAddedEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.SendWelcomeAndChangePasswordEmailPort;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;
import org.expropiados.cloudservices.security.modules.login.domain.roles.User;
import org.expropiados.cloudservices.security.thirdparty.SendGridExternalService;

import java.util.Map;

@EmailAdapter
@RequiredArgsConstructor
public class AccessSendGridEmailAdapter implements SendRoleAddedEmailPort, SendWelcomeAndChangePasswordEmailPort, SendResetPasswordEmailPort {

    private final SendGridExternalService sendGridExternalService;
    private final SendGridConfig sendGridConfig;

    @Override
    public void sendRoleAddedEmail(User user, String email, UserRole role) {
        var fullNames = user.fullnames();
        var rolesAddedTemplatedId = sendGridConfig.getTemplates().getRoleAddedId();
        var frontendHost = sendGridConfig.getVariables().getFrontendHost();

        var data = Map.of(
                "username", fullNames,
                "role", role.getValue(),
                "frontend_host", frontendHost);

        sendGridExternalService.sendEmail(email, data, rolesAddedTemplatedId);
    }

    @Override
    public void sendWelcomeAndChangePasswordEmail(User user, String email, String changePasswordToken) {
        var fullnames = user.fullnames();
        var welcomeTemplateId = sendGridConfig.getTemplates().getWelcomeId();
        var confirmAccountUrl = sendGridConfig.getVariables().getConfirmAccountUrl() + changePasswordToken;

        var data = Map.of(
                "username", fullnames,
                "confirm_account_url", confirmAccountUrl);

        sendGridExternalService.sendEmail(email, data, welcomeTemplateId);
    }

    @Override
    public void sendResetPasswordEmailPort(User user, String email, String changePasswordToken) {
        var fullNames = user.fullnames();
        var resetPasswordTemplateId = sendGridConfig.getTemplates().getResetPasswordId();
        var resetPasswordUrl = sendGridConfig.getVariables().getResetPasswordUrl() + changePasswordToken;

        var data = Map.of(
                "username", fullNames,
                "reset_password_url", resetPasswordUrl);

        sendGridExternalService.sendEmail(email, data, resetPasswordTemplateId);
    }
}
