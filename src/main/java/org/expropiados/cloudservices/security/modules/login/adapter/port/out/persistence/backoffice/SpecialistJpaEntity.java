package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data @Entity
@Table(name= "SPECIALIST", schema = "backoffice")
public class SpecialistJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specialist_id")
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name = "is_blocked")
    private Boolean isBlocked;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserJpaEntity user;
}
