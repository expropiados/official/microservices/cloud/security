package org.expropiados.cloudservices.security.modules.login.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.in.ForgotPasswordUseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.out.GetUserByEmailPort;
import org.expropiados.cloudservices.security.modules.login.application.port.out.SendResetPasswordEmailPort;
import org.expropiados.cloudservices.security.modules.token.application.DecodeChangePasswordTokenService;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateChangePasswordTokenUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@UseCase
@RequiredArgsConstructor
public class ForgotPasswordService implements ForgotPasswordUseCase {

    private final Logger logger = LoggerFactory.getLogger(DecodeChangePasswordTokenService.class);
    private final GetUserByEmailPort getUserByEmailPort;
    private final GenerateChangePasswordTokenUseCase generateChangePasswordTokenUseCase;
    private final SendResetPasswordEmailPort sendResetPasswordEmailPort;

    @Override
    public void forgotPassword(String email) {
        var user = getUserByEmailPort.getUserByEmail(email);

        if (user.isEmpty()) {
            logger.info("Trying to reset password of email not found '{}'", email);
            return;
        }

        var token = generateChangePasswordTokenUseCase.generateChangePasswordToken(user.get().fullnames(), email);
        sendResetPasswordEmailPort.sendResetPasswordEmailPort(user.get(), email, token);
    }
}
