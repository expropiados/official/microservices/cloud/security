package org.expropiados.cloudservices.security.modules.login.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.WebAdapter;
import org.expropiados.cloudservices.security.modules.login.application.port.in.LoginUseCase;
import org.expropiados.cloudservices.security.modules.login.domain.UserTokens;
import org.expropiados.cloudservices.security.modules.login.domain.AccessCredentials;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class LoginController {
    private final LoginUseCase useCase;

    @PostMapping("/login")
    public UserTokens login(@RequestBody AccessCredentials credentials) {
        return useCase.login(credentials);
    }
}
