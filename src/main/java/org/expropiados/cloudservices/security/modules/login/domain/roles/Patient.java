package org.expropiados.cloudservices.security.modules.login.domain.roles;

import lombok.Value;

import java.util.UUID;

@Value
public class Patient {
    Long id;
    UUID uuid;
    User user;
    Boolean isBlocked;
    Long companyId;
    String companyName;
}
