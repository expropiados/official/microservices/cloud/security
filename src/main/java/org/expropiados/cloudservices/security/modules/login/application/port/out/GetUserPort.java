package org.expropiados.cloudservices.security.modules.login.application.port.out;

import org.expropiados.cloudservices.security.modules.login.domain.roles.User;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;

public interface GetUserPort {
    User getUser(String email, UserRole role);
}
