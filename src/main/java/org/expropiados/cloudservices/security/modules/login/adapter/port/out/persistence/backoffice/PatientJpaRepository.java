package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatientJpaRepository extends JpaRepository<PatientJpaEntity, Long> {
    Boolean existsPatientJpaEntityByEmail(String email);
    Optional<PatientJpaEntity> findByEmail(String email);
    Optional<PatientJpaEntity> findByEmailAndIsBlocked(String email, Boolean isBlocked);;
}
