package org.expropiados.cloudservices.security.modules.login.application.port.in;

import org.expropiados.cloudservices.security.modules.login.domain.UserTokens;
import org.expropiados.cloudservices.security.modules.login.domain.AccessCredentials;

public interface LoginUseCase {
    UserTokens login(AccessCredentials credentials);
}
