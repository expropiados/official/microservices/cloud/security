package org.expropiados.cloudservices.security.modules.login.domain;

import lombok.Value;

import java.util.UUID;

@Value
public class RoleIds {
    Long specialistId;
    UUID specialistUuid;

    Long patientId;
    UUID patientUuid;

    Long clientSupervisorId;
    UUID clientSupervisorUuid;
}
