package org.expropiados.cloudservices.security.modules.login.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.helpers.PasswordHelper;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.in.ChangePasswordUseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.out.StorePasswordPort;
import org.expropiados.cloudservices.security.modules.token.application.port.in.DecodeChangePasswordTokenUseCase;
import org.springframework.security.crypto.bcrypt.BCrypt;

@UseCase
@RequiredArgsConstructor
public class ChangePasswordService implements ChangePasswordUseCase {

    private final DecodeChangePasswordTokenUseCase changePasswordTokenUseCase;
    private final StorePasswordPort storePasswordPort;
    private final PasswordHelper passwordHelper;

    @Override
    public void changePassword(String token, String newPassword) {
        var tokenBody = changePasswordTokenUseCase.decodeChangePasswordToken(token);

        var hashedPassword = passwordHelper.generateHashedPassword(newPassword);
        storePasswordPort.storePassword(tokenBody.getEmail(), hashedPassword);
    }
}
