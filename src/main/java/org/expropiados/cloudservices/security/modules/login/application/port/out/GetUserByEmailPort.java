package org.expropiados.cloudservices.security.modules.login.application.port.out;

import org.expropiados.cloudservices.security.modules.login.domain.roles.User;

import java.util.Optional;

public interface GetUserByEmailPort {
    Optional<User> getUserByEmail(String email);
}
