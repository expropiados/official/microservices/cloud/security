package org.expropiados.cloudservices.security.modules.login.domain.roles;

import lombok.Builder;
import lombok.Value;

import java.util.Optional;

@Value
@Builder
public class AllRoles {
    String providerCompanyName;
    String userImageUrl;
    Admin admin;
    Optional<Patient> patient;
    Optional<Specialist> specialist;
    Optional<ClientSupervisor> clientSupervisor;
    Optional<ProviderSupervisor> providerSupervisor;
}
