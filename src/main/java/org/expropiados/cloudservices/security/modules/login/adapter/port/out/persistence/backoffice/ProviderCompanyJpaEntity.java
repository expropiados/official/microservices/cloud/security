package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@ToString
@Getter @Setter
@RequiredArgsConstructor
@Entity @Table(name = "PROVIDER_COMPANY", schema = "backoffice")
public class ProviderCompanyJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name = "business_name", length = 150, nullable = false)
    private String businessName;

    @Column(name = "responsible_email", length = 150)
    private String responsibleEmail;

    @Column(name = "responsible_name", length = 150)
    private String responsibleName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ProviderCompanyJpaEntity that = (ProviderCompanyJpaEntity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 86937865;
    }
}
