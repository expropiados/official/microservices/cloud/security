package org.expropiados.cloudservices.security.modules.login.application;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cloudservices.security.hexagonal.errors.NotFoundException;
import org.expropiados.cloudservices.security.hexagonal.errors.UserInputException;

import java.io.Serializable;

@Getter
public class UserNotRegisteredException extends RuntimeException implements NotFoundException {
    private final String code = "SEC_LOG_004";
    private final String message;
    private final Object data;

    @Value
    static class Data implements Serializable {
        String email;
    }

    public UserNotRegisteredException(String email) {
        super();
        this.message = String.format("User with email '%s' is not registered in the system", email);
        this.data = new UserNotRegisteredException.Data(email);
    }
}
