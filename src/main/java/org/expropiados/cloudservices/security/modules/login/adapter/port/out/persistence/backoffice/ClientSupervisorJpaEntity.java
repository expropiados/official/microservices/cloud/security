package org.expropiados.cloudservices.security.modules.login.adapter.port.out.persistence.backoffice;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name= "CLIENT_SUPERVISOR", schema = "backoffice")
public class ClientSupervisorJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_supervisor_id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name = "client_company_id", nullable = false)
    private Long clientCompanyId;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name = "is_blocked")
    private Boolean isBlocked;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserJpaEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_company_id", insertable = false, updatable = false)
    private CompanyJpaEntity clientCompany;
}
