package org.expropiados.cloudservices.security.modules.login.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.hexagonal.UseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.in.RegisterUseCase;
import org.expropiados.cloudservices.security.modules.login.application.port.out.*;
import org.expropiados.cloudservices.security.modules.login.domain.UserRole;
import org.expropiados.cloudservices.security.modules.token.application.port.in.GenerateChangePasswordTokenUseCase;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class RegisterRoleEmailService implements RegisterUseCase {

    private final GetCurrentRolesForUserEmailPort getCurrentRolesForUserEmailPort;
    private final GetUserPort getUserPort;
    private final RegisterEmailPort registerEmailPort;
    private final SendWelcomeAndChangePasswordEmailPort sendWelcomeAndChangePasswordEmailPort;
    private final SendRoleAddedEmailPort sendRoleAddedEmailPort;
    private final GenerateChangePasswordTokenUseCase generateChangePasswordUseCase;

    @Override
    public void registerRoleEmail(String email, UserRole role) {
        var roles = getCurrentRolesForUserEmailPort.getCurrentRolesForUserEmail(email);

        if (!roles.contains(role)) {
            throw new RoleNotFoundForUserException(email, role);
        }

        var user = getUserPort.getUser(email, role);

        if (isNewUser(roles)) {
            registerEmailPort.registerEmail(email);
            var changePasswordToken = generateChangePasswordUseCase.generateChangePasswordToken(user.fullnames(), email);
            sendWelcomeAndChangePasswordEmailPort.sendWelcomeAndChangePasswordEmail(user, email, changePasswordToken);
            return;
        }

        sendRoleAddedEmailPort.sendRoleAddedEmail(user, email, role);
    }

    private boolean isNewUser(List<UserRole> roles) {
        return roles.size() == 1;
    }
}
