package org.expropiados.cloudservices.security.modules.token.application.port.in;

import org.expropiados.cloudservices.security.modules.token.domain.ChangePasswordTokenBody;

public interface DecodeChangePasswordTokenUseCase {
    ChangePasswordTokenBody decodeChangePasswordToken(String token);
}
