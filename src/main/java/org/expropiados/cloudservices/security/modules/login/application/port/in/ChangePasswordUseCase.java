package org.expropiados.cloudservices.security.modules.login.application.port.in;

public interface ChangePasswordUseCase {
    void changePassword(String token, String newPassword);
}
