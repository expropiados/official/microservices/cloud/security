package org.expropiados.cloudservices.security.helpers;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.expropiados.cloudservices.security.config.security.JwtConfig;
import org.expropiados.cloudservices.security.hexagonal.HelperAdapter;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Map;

@HelperAdapter
@RequiredArgsConstructor
public class PasswordHelper {
    public String generateHashedPassword(String newPassword) {
        return BCrypt.hashpw(newPassword, BCrypt.gensalt());
    }

    public boolean areSamePasswords(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }
}
